### 26 网格转换
为使用`blockMesh`之外的程序生成的网格，openFOAM的使用指南中列出了下列转换工具：
- *fluentMeshToFoam*
- *starToFoam*
- *gambitToFoam*
- *ideasToFoam*
- *cfx4ToFoam*

从转换工具的名称即可知道其功能。

**建议**

**始终以ASCII格式写入**

当前安装的OpenFOAM和其他任何软件之间的任何数据交换最好使用ASCII格式处理。

#### 26.1 *fluentMeshToFoam* 与 *fluent3DMeshToFoam*
*fluentMeshToFoam* 将以`*.msh`格式存储的网格转换为`OpenFOAM`格式。具体而言，*fluentMeshToFoam*仅用于转换二维网格，三维网格可以使用*fluent3DMeshToFoam*进行转换。

转换工具需要把`*.msh`文件的路径作为参数。其将网格以`OpenFOAM`格式保存在`constant/polymesh`目录中。

如果转换工具是从当前算例目录以外的目录调用的，那么算例目录的路径必须通过一个附加参数指定。见第12.6节。

如果网格是使用米以外的其他尺寸创建的，则可以使用命令行参数`-scale`来更正缩放比例。OpenFOAM期望网格数据以米表示。
所有其他可能的选项都可以用命令行参数fluentMeshToFoam-help显示。

#### 26.2 *ideasToFoam*
*ideasToFoam*通常用于将`UNV`格式的网格转换为`OpenFOAM`格式，`Salome`是一个著名的网格划分软件，其以`UNV`格式输出网格，详见第23节。

#### 26.3 陷阱：长度单位
第三方生成的网格单位可能是毫米，而不是OpenFOAM默认的米。点坐标将由OpenFOAM解释为以米为单位，即 $P_{in\_file}$=(120, 240, *left*(120 0) mm 将被OpenFOAM读取为$P_{in\_mesh}$=(120, 240, 0) m。因此导入的网格将按1000的比例缩放。


> ![](images/53.PNG)
> 总是运行checkMesh，最好是使用它的选项 -allGeometry 和 -allTopology，首先检查网格质量，然后检查网格边界框。边界框将以米为单位，如同OpenFOAM中其他任何长度一样。这是一个检查毫米与米的机会。


清单150显示了checkMesh输出的相关行。除非我们计算气象流量，否则以公里为单位的计算域似乎有点偏差。
``` bash
Checking geometry ...
Overall domain bounding box ( -752.264 -325 -684.294) (752.264 1400 3754.35)
```
代码150：网格的边界框